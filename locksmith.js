var os = Npm.require("os");

var Locks = new Meteor.Collection('locks');

function Lock(processName, lockName, expirationDate) {
    this.processName = processName
    this.lockName = lockName
    this.expirationDate = expirationDate
}

Lock.prototype.unlock = function() {
    Locks.remove({
        _id: this.lockName,
        process: this.processName
    })
}

Lock.prototype.isLocked = function() {
    var currentTime = new Date()
    if (currentTime > this.expirationDate) {
        return false
    }
    return true
}

Lock.prototype.renew = function() {
    if (!this.isLocked()) {
        // lock already expired
        // try to acquire new lock instead
        var newLock = LockSmith.lock(this.lockName)
        if (newLock != null) {
            this.processName = newLock.processName
            this.lockName = newLock.lockName
            this.expirationDate = newLock.expirationDate
            return true
        }

        // failed to acqurei new lock
        return false
    }

    // extend lock
    var now = new Date()
    var newExpirationDate = new Date()
    newExpirationDate.setSeconds(now.getSeconds() + LockSmith.lockTTL)
    var numAffected = Locks.update({
        _id: this.lockName,
        process: this.processName
    }, {
        $set: {
            expires: newExpirationDate
        }
    })

    if (numAffected != 1) {
    	// failed to renew lock
    	return false
    }

    this.expirationDate = newExpirationDate
    return true
}

LockSmith = {
    hostName: os.hostname(),
    lockTTL: 60 // seconds
}

LockSmith.lock = function(lockName) {
    // remove outdated locks
    Locks.remove({
        _id: lockName,
        expires: {
            $lt: new Date()
        }
    })

    // try to lock
    var creationDate = new Date()
    var expirationDate = new Date()
    expirationDate.setSeconds(creationDate.getSeconds() + LockSmith.lockTTL)

    try {
        Locks.insert({
            _id: lockName,
            created: creationDate,
            expires: expirationDate,
            process: LockSmith.hostName,
        })
    } catch (e) {
        if (e.name == "MongoError" && e.code == 11000) {
            // failed to acquire lock because another lock present
            return null
        } else {
            throw e;
        }
    }

    // return lock object
    return new Lock(LockSmith.hostName, lockName, expirationDate)
}