# Distributed Lock Management Service for Meteor

Distributed Lock Manager utilizing MongoDB's unique index to create and manage locks between Meteor processes

### Lock Life Cycle
```js
LockSmith.lockTTL = 30 // set lock TTL in seconds, defaults to 60

var lock = LockSmith.lock("testlock") // create lock
if (lock != null) {
    console.log("lock acquired")
}
if (lock.isLocked()) {=
    console.log("lock valid")
}

// create duplicate lock
var lock2 = LockSmith.lock("testlock")
if (lock2 == null) {
    console.log("failed to acquire duplicate lock")
}

if (lock.renew()) {
    console.log("lock renewed successfully")
}

lock.unlock() // release lock after usgae
```

### Typical Usage
```js
// start critical section between processes
var lock = LockSmith.lock("testlock") // create lock
while(true) {
    if (lock.isLocked()) {
        // do something
    }
    if (lock.renew()) break // renew lock after each work
}
lock.unlock()
```
## License
Licensed under the MIT License.