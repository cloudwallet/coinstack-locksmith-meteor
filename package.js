Package.describe({
    name: 'shepelt:locksmith',
    version: '1.0.1_1',
    summary: 'Distributed Lock Manager',
    git: 'https://bitbucket.org/cloudwallet/coinstack-locksmith-meteor.git',
    documentation: 'README.md'
});

Package.onUse(function(api) {
    api.versionsFrom('1.1.0.2');
    api.addFiles('locksmith.js', ['server']);
    api.export("LockSmith", ['server']);
});

Package.onTest(function(api) {
    api.use(['tinytest', 'test-helpers'], ['server']);
    api.addFiles(['locksmith.js', 'locksmith_test.js'], ['server']);
});
