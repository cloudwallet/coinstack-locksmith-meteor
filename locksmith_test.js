Tinytest.addAsync('create lock and let it expire', function(test, next) {
    LockSmith.lockTTL = 2
    test.isNotNull(LockSmith)
    var lock = LockSmith.lock("testlock")
    test.isNotNull(lock)
    test.isTrue(lock.isLocked())

    // create duplicate lock
    var lock2 = LockSmith.lock("testlock")
    test.isNull(lock2)

    // test lock after TTL
    Meteor.setTimeout(function() {
        test.isFalse(lock.isLocked())

        var lock3 = LockSmith.lock("testlock")
        test.isNotNull(lock3)

        // lock3.unlock()

        next()
    }, 2500)
});

Tinytest.addAsync('create lock and renew', function(test, next) {
    LockSmith.lockTTL = 2
    test.isNotNull(LockSmith)
    var lock = LockSmith.lock("testlock2")
    test.isNotNull(lock)
    test.isTrue(lock.isLocked())

    // create duplicate lock
    var lock2 = LockSmith.lock("testlock2")
    test.isNull(lock2)

    // test lock after TTL
    Meteor.setTimeout(function() {
        test.isTrue(lock.isLocked())

        test.isTrue(lock.renew())


        Meteor.setTimeout(function() {
            test.isTrue(lock.isLocked())
            next()
        }, 1500)

    }, 500)
});